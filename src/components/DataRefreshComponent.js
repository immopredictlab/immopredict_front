import { useEffect, useContext } from 'react';
import { store } from '../context/store'

export default function DataRefreshComponent() {
    const globalState = useContext(store);
    const { dispatch } = globalState;

    useEffect(() => {
        normalRoutine()
        refreshBuildings()
        let interval = setInterval(normalRoutine, 30 * 1000)
        let interval2 = setInterval(refreshBuildings, 1000 * 60 * 15)

        return () => {
            clearInterval(interval)
            clearInterval(interval2)
        }
    }, [])


    const normalRoutine = () => {
        checkStatus()
        refreshMetadata()
    }

    const checkStatus = (() => {
        fetch(`${process.env.REACT_APP_BACKENDROUTE}/status`)
            .then(r => r.json())
            .then(result => {
                dispatch({ type: 'updateProgramStatus', data: result })
            })
    })

    const refreshBuildings = () => {
        fetch(`${process.env.REACT_APP_BACKENDROUTE}/buidlings/list`)
            .then(r => r.json())
            .then(result => {
                dispatch({ type: 'updateBatiments', data: result })
            })
    }

    const refreshMetadata = () => {
        fetch(`${process.env.REACT_APP_BACKENDROUTE}/metadata`)
            .then(r => r.json())
            .then(result => {
                dispatch({ type: 'updateMetadata', data: result })
                dispatch({ type: 'tryInitUserSetPercent', data: result.promoteurPercent })
            })
    }

    return null
}