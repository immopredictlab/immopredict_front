export const columns = [
    {
        label: 'Nom',
        value: ((data) => data.name),
    },
    {
        label: "Prix d'achat",
        value: ((data) => data.prix),
        align: 'center',
        format: (value) => parseInt(value.toFixed(0)).toLocaleString('fr'),
        toolTip: "Prix d'achat direct"
    },
    {
        label: "Renta AL",
        value: ((data, br) => br?.alDayRentability),
        align: 'center',
        format: (value) => parseInt(value.toFixed(0)).toLocaleString('fr'),
        toolTip: "Nombre de jour avant rentabilité et acheté puis loué"
    },
    {
        label: 'Vente',
        value: ((data, br) => br?.finalSellPrice),
        align: 'center',
        format: (value) => parseInt(value.toFixed(0)).toLocaleString('fr'),
        toolTip: "Prix de vente immédiat, prend en compte le taux promoteur"
    },

    {
        label: 'Cout AC',
        value: ((data, br) => br?.ac?.cost),
        align: 'center',
        format: (value) => parseInt(value.toFixed(0)).toLocaleString('fr'),
        toolTip: "Cout si construit, puis vendu. Si non constructible, est égale au prix d'achat"
    },
    {
        label: 'Renta ACV',
        value: ((data, br) => br?.ac?.rentability),
        align: 'center',
        format: (value) => parseInt(value.toFixed(0)).toLocaleString('fr'),
        toolTip: "Bénéfice net si AC. Prend en compte le taux promoteur"
    },
    {
        label: 'Renta ACL',
        value: ((data, br) => br?.ac?.rentDayRentability),
        align: 'center',
        format: (value) => parseInt(value.toFixed(0)).toLocaleString('fr'),
        toolTip: "Nombre de jour avant rentabilité si AC. Prend en compte les charges"
    },

    {
        label: 'Cout AE',
        value: ((data, br) => br?.ae?.cost),
        align: 'center',
        format: (value) => parseInt(value.toFixed(0)).toLocaleString('fr'),
        toolTip: "Cout si acheté puis embelli. Si non constructible, est égale au prix d'achat"

    },
    {
        label: 'Renta AEV',
        value: ((data, br) => br?.ae?.rentability),
        align: 'center',
        format: (value) => parseInt(value.toFixed(0)).toLocaleString('fr'),
        toolTip: "Bénéfice net si vendu apres AE. Prend en compte le taux promoteur"

    },
    {
        label: 'Renta AEL',
        value: ((data, br) => br?.ae?.rentDayRentability),
        align: 'center',
        format: (value) => parseInt(value.toFixed(0)).toLocaleString('fr'),
        toolTip: "Nombre de jour avant rentabilité si AE. Prend en compte les charges"

    },

];
