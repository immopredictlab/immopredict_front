import React, { useContext, useEffect, useState } from 'react';
import { store } from '../context/store'
import Grid from '@mui/material/Grid';
import Chip from '@mui/material/Chip';



import Slider from '@mui/material/Slider';




export default function PromoteurComponent() {

    const globalState = useContext(store);
    const { dispatch } = globalState;

    const [sliderVal, setsliderVal] = useState(-1);

    useEffect(() => {
        if (sliderVal === -1 && globalState.state.userSetPromoteurPercent !== -1) {
            setsliderVal(globalState.state.userSetPromoteurPercent * 100)
        }
    }, [globalState.state.userSetPromoteurPercent])

    const updateSlider = (e, newval) => {
        setsliderVal(newval)
    }

    const finalPercentSelected = () => {
        var res = sliderVal / 100.0
        res = Math.round(res * 100.0) / 100.0
        dispatch({ type: 'updateUserSetPercent', data: res })
    }

    return (
        <div>
            <div className={"percentdisplay"}>
                <h3>Taux promoteur {Math.round(globalState.state.userSetPromoteurPercent * 100)} % <Chip variant="outlined" color="success" size="small" label={`Live: ${globalState.state.metadata.promoteurPercent * 100}%`} /></h3>

                <Grid container direction="row" justifyContent="center" alignItems="center"  onClick={() => finalPercentSelected()}>

                    <Grid item xs={12} sm={4}>
                        <Slider min={70} max={94} defaultValue={50} value={sliderVal} aria-label="Default" valueLabelDisplay="auto" onChange={updateSlider} marks={[{value:70,label:'70%'},{value:94,label:'94%'}]} />
                    </Grid>
                </Grid>
            </div>
        </div>
    )

}