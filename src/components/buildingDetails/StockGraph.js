import React, { useState, useEffect, useContext } from 'react';
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import moment from 'moment'

import {
    ComposedChart,
    Line,
    Area,
    Bar,
    XAxis,
    YAxis,
    CartesianGrid,
    Tooltip,
    Legend,
    ResponsiveContainer,
} from 'recharts';

export default function StockGraph(props) {

    const [stockData, setStockData] = useState([])
    const [minMaxGraph, setminMaxGraph] = useState({ min: 0, max: 100 })

    const [boughtData, setboughtData] = useState([])

    useEffect(() => {
        if (props?.target?.id == null || props?.target?.id === undefined) return
        refreshData()
    }, [props.target])

    if (!props?.target?.label) return (null)

    const refreshData = (() => {
        fetch(`${process.env.REACT_APP_BACKENDROUTE}/buidlings/${props.target.id}/dispo`)
            .then(r => r.json())
            .then(result => {
                computeDispoChart(result)
            })

        fetch(`${process.env.REACT_APP_BACKENDROUTE}/buidlings/${props.target.id}/bought`)
            .then(r => r.json())
            .then(result => {
                computeBoughtChart(result)
            })
    })

    const computeBoughtChart = (result) => {
        let finalData = result.map(d => {
            return {
                ...d,
                dayTs: d.dayTs * 1000,
            }
        })

        setboughtData(finalData)
    }

    const computeDispoChart = (result) => {
        let finalData = result.map(d => {
            return {
                dispo: d.data[1].dispo,
                time: d.data[1].time * 1000,
            }
        })
        setStockData(finalData)

        let tempArray = finalData.map(f => f.dispo);
        let min = Math.min(...tempArray)
        let max = Math.max(...tempArray)

        setminMaxGraph({
            min: min - 1,
            max: max + 1
        })
    }

    const renderTooltipContent = (o) => {
        const { payload } = o;
        let data = payload[0]?.payload
        if (!data) return null;
        let locdate = moment(data.time).format('dd DD/MM HH:mm')
        return (<Paper p={2}>
            <Typography>{locdate}</Typography>
            <Typography>Dispo {data.dispo}</Typography>
        </Paper>
        );
    };

    const renderTooltipContentBought = (o) => {
        const { payload } = o;
        let data = payload[0]?.payload
        if (!data) return null;
        let locdate = moment(data.dayTs).format('dd DD/MM')
        return (<Paper p={2}>
            <Typography>{locdate}</Typography>
            <Typography>Quantité acheté {data.quantityBought}</Typography>
        </Paper>
        );
    };

    return (
        <Box>
            <Box>
                <Typography variant="h5">Analyse des stocks: {props.target.label}</Typography>
                <Grid item xs={12} sx={{ height: '400px' }} mt={3}>
                    <ResponsiveContainer width="100%" height="100%">
                        <ComposedChart
                            width={500}
                            height={400}
                            data={stockData}

                        >
                            <CartesianGrid stroke="#f5f5f5" />
                            <XAxis dataKey="time" tickFormatter={timeStr => moment(timeStr).format('DD/MM HH[h]')} />
                            <YAxis domain={[minMaxGraph.min, minMaxGraph.max]} />
                            <Tooltip content={renderTooltipContent} />
                            <Bar dataKey="dispo" fill="orange" />
                        </ComposedChart>
                    </ResponsiveContainer>
                </Grid>
            </Box>
            <Box mt={5}>
                <Typography variant="h5">Projection des achats</Typography>
                <Typography variant="body1">Ceci est une projection approximative du nombre de bien acheté par jour</Typography>
                <Grid item xs={12} sx={{ height: '400px' }} mt={3}>
                    <ResponsiveContainer width="100%" height="100%">
                        <ComposedChart
                            width={500}
                            height={400}
                            data={boughtData}

                        >
                            <CartesianGrid stroke="#f5f5f5" />
                            <XAxis dataKey="dayTs" tickFormatter={timeStr => moment(timeStr).format('DD/MM')} />
                            <YAxis />
                            <Tooltip content={renderTooltipContentBought} />
                            <Bar dataKey="quantityBought" fill="red" />
                        </ComposedChart>
                    </ResponsiveContainer>
                </Grid>
            </Box>
        </Box>
    )
}
