import React, { useState, useEffect, useContext } from 'react';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import { LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';
import moment from 'moment'
import Paper from '@mui/material/Paper';



export default function PriceGraph(props) {

    const [priceData, setPriceData] = useState([])

    React.useEffect(() => {
        if (props?.target?.id == null || props?.target?.id === undefined) return
        refreshData()
    }, [props.target])

    if (!props?.target?.label) return (null)


    const refreshData = (() => {
        fetch(`${process.env.REACT_APP_BACKENDROUTE}/buidlings/${props.target.id}/price`)
            .then(r => r.json())
            .then(result => {
                computePriceData(result)
            })
    })

    const computePriceData = (result) => {
        let finalData = result.map(d => {
            return {
                ...d,
                time: d.time * 1000,
            }
        })

        setPriceData(finalData)
    }

    
    const renderTooltipContent = (o) => {
        const { payload } = o;
        let data = payload[0]?.payload
        if (!data) return null;
        let locdate = moment(data.time).format('dd DD/MM HH:mm')
        return (<Paper p={2}>
            <Typography>{locdate}</Typography>
            <Typography>Prix {data.prix}</Typography>
        </Paper>
        );
    };


    return (
        <Box>
            <Typography variant="h5">Analyse des prix: {props.target.label}</Typography>
            <Grid item xs={12} sx={{ height: '400px' }} mt={3}>
                <ResponsiveContainer width="100%" height="100%">
                    <LineChart
                        width={500}
                        height={300}
                        data={priceData}
                        margin={{
                            top: 5,
                            right: 30,
                            left: 20,
                            bottom: 5,
                        }}
                    >
                        <CartesianGrid strokeDasharray="3 3" />
                        <XAxis dataKey="time" tickFormatter={timeStr => moment(timeStr).format('DD/MM')}/>
                        <YAxis />
                        <Tooltip content={renderTooltipContent} />
                        <Line type="monotone" dataKey="prix" stroke="green" />
                    </LineChart>
                </ResponsiveContainer>
            </Grid>

        </Box>
    )
}

const data = [
    {
        name: 'Page A',
        uv: 4000,
        pv: 2400,
        amt: 2400,
    },
    {
        name: 'Page B',
        uv: 3000,
        pv: 1398,
        amt: 2210,
    },
    {
        name: 'Page C',
        uv: 2000,
        pv: 9800,
        amt: 2290,
    },
    {
        name: 'Page D',
        uv: 2780,
        pv: 3908,
        amt: 2000,
    },
    {
        name: 'Page E',
        uv: 1890,
        pv: 4800,
        amt: 2181,
    },
    {
        name: 'Page F',
        uv: 2390,
        pv: 3800,
        amt: 2500,
    },
    {
        name: 'Page G',
        uv: 3490,
        pv: 4300,
        amt: 2100,
    },
];