
import React, { useState, useEffect } from 'react';
import { AreaChart, Area, XAxis, YAxis, CartesianGrid, Tooltip, ResponsiveContainer } from 'recharts';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import Paper from '@mui/material/Paper';
import Typography from '@mui/material/Typography';
import moment from 'moment'


export default function PromoteurGraph() {
    const [webData, setWebData] = useState([])

    useEffect(() => {
        reloadData()
    }, [])


    const reloadData = () => {
        fetch(`${process.env.REACT_APP_BACKENDROUTE}/history/promoteur`)
            .then(r => r.json())
            .then(result => {

                let finalData = result.map(d => {
                    return {
                        ...d,
                        promoteurPercent: d.promoteurPercent * 100,
                        time: d.time * 1000,
                    }
                })

                setWebData(finalData)
            })
    }

 

    const renderTooltipContent = (o) => {
        const { payload } = o;
        let data = payload[0]?.payload
        if (!data) return null;
        let locdate = moment(data.time).format('DD/MM HH:mm')
        return (
            <Paper>
                <Typography>{locdate}</Typography>
                <Typography>{data.promoteurPercent} %</Typography>
            </Paper>
        );
    };

    return (
        <Grid item xs={12} sx={{ height: '300px' }}>
            <ResponsiveContainer width="100%" height="100%">

                <AreaChart
                    width={500}
                    height={400}
                    data={webData}
                    margin={{
                        top: 10,
                        right: 30,
                        left: 0,
                        bottom: 0,
                    }}
                >
                    <CartesianGrid strokeDasharray="1 8" />
                    <XAxis dataKey="time" tickFormatter={timeStr => moment(timeStr).format('DD/MM HH:mm')} scale="time" />
                    <YAxis domain={[70, 94]} />
                    <Tooltip content={renderTooltipContent} />
                    <Area type="monotone" dataKey="promoteurPercent" stroke="#8884d8" fill="#8884d8" />
                </AreaChart>
            </ResponsiveContainer>
        </Grid>
    )
}