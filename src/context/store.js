import React, { createContext, useReducer } from 'react';

const initialState = {
    programStatus: {
        isReady: false,
        lastDataUpdate: 0
    },
    buildingsList: [],
    metadata: {
        promoteurPercent: -1,
    },
    userSetPromoteurPercent: -1,
    rentability: []

};

const store = createContext(initialState);

function reducer(state, action) {
    switch (action.type) {
        case 'updateProgramStatus':
            return {
                ...state,
                programStatus: action.data
            };
        case 'updateBatiments':
            return {
                ...state,
                buildingsList: action.data
            };
        case 'updateMetadata':
            return {
                ...state,
                metadata: action.data
            };
        case 'updateRentability':
            return {
                ...state,
                rentability: action.data
            };
        case 'updateUserSetPercent':
            return {
                ...state,
                userSetPromoteurPercent: action.data
            };
        case 'tryInitUserSetPercent':
            return {
                ...state,
                userSetPromoteurPercent: state.userSetPromoteurPercent === -1 ? action.data : state.userSetPromoteurPercent
            };
        default:
            throw new Error(action.type);
    }
}

const StateProvider = ({ children }) => {
    const [state, dispatch] = useReducer(reducer, initialState);
    return <store.Provider value={{ state, dispatch }}>{children}</store.Provider>;
};

export { store, StateProvider }