import React, { useEffect, useContext } from 'react';
import { store } from '../context/store'
import { BuidlingRentability } from '../models/BuidlingRentability';

export default function PriceCalculatorService() {

    const globalState = useContext(store);
    const { dispatch } = globalState;

    useEffect(() => {
        refreshRentability()
    }, [globalState.state.buildingsList, globalState.state.userSetPromoteurPercent])


    const refreshRentability = () => {
        let buildingsRentabilities = computeBuidlingsData()
        dispatch({ type: 'updateRentability', data: buildingsRentabilities })
    }

    const getBuildingById = (buildingId) => {
        return globalState.state.buildingsList.find(b => b.id === buildingId)
    }

    const computeBuidlingsData = () => {
        return globalState.state.buildingsList
            .filter(b => !b.isTerrain)
            .map(b => computeBuildingData(b))
    }

    const computeBuildingData = (buidling) => {

        const result = new BuidlingRentability(buidling.id)
        let sellPrice = globalState.state.userSetPromoteurPercent * buidling.prix;

        result.finalSellPrice = sellPrice

        //Permit to check if it's chamber who can't be constructed
        if (buidling.terrainId != null) {
            let cost = getBuildingById(buidling.terrainId).prix + buidling.buildPrice;

            result.ac = {
                cost: cost,
                rentability: sellPrice - cost,
                rentDayRentability: buidling.loyer > 0 ? Math.round(cost / (buidling.loyer - buidling.charges)) : null
            }
        } else {
            result.ac = null;
        }


        if (buidling.nextBuildingId != null) {
            let nextBuidling = getBuildingById(buidling.nextBuildingId)

            let cost = buidling.prix + buidling.upgrapdePrice
            let nextSellPrice = globalState.state.userSetPromoteurPercent * nextBuidling.prix;
            result.ae = {
                cost: cost,
                rentability: nextSellPrice - cost,
                rentDayRentability: Math.round(cost / (nextBuidling.loyer - nextBuidling.charges))
            }
        }

        if (buidling.loyer > 0) {
            result.alDayRentability = Math.round(buidling.prix / (buidling.loyer - buidling.charges))
        }

        return result;

    }



    return null
}


