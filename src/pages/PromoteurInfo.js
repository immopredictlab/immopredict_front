import PromoteurGraph from "../components/promoteur/PromoteurGraph"
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';




export default function PromoteurInfo() {

    return (
        <Box>
            <Typography variant="h3" noWrap component="div" sx={{ mb: 5 }} >
                Variation promoteur
            </Typography>
            <PromoteurGraph />
        </Box>

    )
}