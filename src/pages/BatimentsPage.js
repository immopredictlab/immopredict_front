import React, { useState, useEffect, useContext } from 'react';

import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import SearchIcon from '@mui/icons-material/Search';
import Grid from '@mui/material/Grid';
import Autocomplete from '@mui/material/Autocomplete';
import TextField from '@mui/material/TextField';
import StockGraph from '../components/buildingDetails/StockGraph';
import PriceGraph from '../components/buildingDetails/PriceGraph';

import { store } from '../context/store'


export default function BatimentsPage() {

    const globalState = useContext(store);

    const [batimentTarget, setBatimentTarget] = React.useState(null);
    const [batimentListInput, setbatimentListInput] = React.useState([]);

    useEffect(() => {

        let inputs = globalState.state.buildingsList.map(b => {
            return {
                id: b.id,
                label: b.name
            }
        })
        setbatimentListInput(inputs)
    }, [globalState.state.buildingsList])

    return (
        <Box>
            <Typography variant="h4">
                <b>Analyse avancée des batiments</b>
            </Typography>

            <Grid item md={6} mt={3}>
                <Autocomplete
                    options={batimentListInput}
                    renderInput={(params) => <TextField {...params} label="Batiment" />}
                    onChange={(event, newValue) => {
                        setBatimentTarget(newValue);
                    }}
                />
            </Grid>

            <Box mt={3}>
                <StockGraph target={batimentTarget} />
            </Box>

            <Box mt={3}>
                <PriceGraph target={batimentTarget} />
            </Box>

        </Box>
    )
}